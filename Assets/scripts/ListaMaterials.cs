using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListaMaterials : MonoBehaviour
{

    public Vector3 min = new Vector3(-5.0f, -5.0f, -5.0f);
    public Vector3 max = new Vector3(5.0f, 5.0f, 5.0f);
    List<GameObject> _list;
    public List<GameObject> targetList;
    private List<Material> materialsList;
    private List<Shader> shadersList;

    // Start is called before the first frame update
    void Start()
    {
        _list = new List<GameObject>();
        targetList = new List<GameObject>(Resources.LoadAll<GameObject>("Targets"));
        materialsList = new List<Material>(Resources.LoadAll<Material>("Materials"));
        shadersList = new List<Shader>(Resources.LoadAll<Shader>("Shaders"));
        for (int i = 0; i < 5; i++)
        {
            var target = new Vector3(
             UnityEngine.Random.Range(min.x, max.x),
             UnityEngine.Random.Range(min.y, max.y),
             UnityEngine.Random.Range(min.z, max.z)
            );
            //rand targetList.Count
            GameObject thisObject = Instantiate(targetList[0], target, Random.rotation);// as GameObject;
            Renderer rend = thisObject.GetComponent<Renderer>();
            rend.material = materialsList[0];
            _list.Add(thisObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var target = new Vector3(
              UnityEngine.Random.Range(min.x, max.x),
              UnityEngine.Random.Range(min.y, max.y),
              UnityEngine.Random.Range(min.z, max.z)
             );
            GameObject thisObject = Instantiate(targetList[0], target, Random.rotation);// as GameObject;
            _list.Add(thisObject);
        }
        if (Input.GetKeyDown("space"))
        {
            //.transform.Translate(
            for (int i = 0; i < _list.Count; i++)
            {
                var target = new Vector3(
                UnityEngine.Random.Range(min.x, max.x),
                UnityEngine.Random.Range(min.y, max.y),
                UnityEngine.Random.Range(min.z, max.z)
               );
                Renderer rend = _list[i].GetComponent<Renderer>();
                //rend.material = materialsList[1];
                rend.material.shader = shadersList[2];
                rend.material.SetColor("_Color", new Color(0, 0, ((float)i+1.0f)/ (float)_list.Count, 1));
                rend.material.SetFloat("Frec", ((float)i)*10.0f);
                //_list[i].transform.Translate(target);
                //_list[i].transform.position += new Vector3(0.0f,0.0f,0.0f);
                _list[i].transform.localScale = target;
                // _list[i].transform.localScale += target;
            }
        }
        if (Input.GetButtonDown("Fire2") && _list.Count > 0)
        {
            GameObject destroyObject = _list[0];
            _list.RemoveAt(0);

            Destroy(destroyObject);

            //Destroy destroyObject;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;
using System.Linq;

public class EvaluacionRegex : MonoBehaviour
{
    Dictionary<string, float> dict = new Dictionary<string, float>();
    string[] clavesDelDiccionario = { "ax", "ay", "az", "gx", "gy", "gz" };

    /*
     *generar un map/dict que contenga los val a usar 
     *Dictionary<string, string> dict =   new Dictionary<string, string>();
     *DESICIOMES O UN PAR STR STR o STR Float
     *filtrar por regex el msj
     */
    // Start is called before the first frame update
    void Start()
    {
        foreach (string key in clavesDelDiccionario)
        {
            dict.Add(key, 999.0f);
        }
        string str = "/estoEsUnaPrueba:36/segundaVariable:valor/3Variable:100.5678//:/@@@/:fssfg/ax:0.50/ay:30.50/az:20.50/gx:0.550/gy:0.0/gz:0.0//:/";
        //string test = "estosdfsrueba:36/segunsfdsdfVariable:valor/afas/afas/gsdgs/";
        //string test2 = "/estosdfsrueba:36/segunsfdsdfVariable:valor/afas/afas/gsdgs/";
        Debug.Log(str);

        Regex rgxClaveValor = new Regex(@"\:");
        Regex rgxGrupos = new Regex(@"\/\w+\:[\w\s\.]+");


        foreach (Match match in rgxGrupos.Matches(str))
        {
            //    index++;

            Debug.Log(String.Format("Found '{0}' at position {1}", match.Value, match.Index));
            string grupo = match.Value.TrimStart('/');
            //Debug.Log(grupo);

            string[] pares = rgxClaveValor.Split(grupo);
            if (pares.Length == 2)
            {
                string clave = pares[0];
                string valor = pares[1];
                //Debug.Log(String.Format("{0} :: {1}", clave, valor));

                //https://learn.microsoft.com/en-us/dotnet/api/system.single.parse?view=net-6.0#system-single-parse(system-string)
                //https://forum.unity.com/threads/float-tryparse-not-working-as-expected.1225824/
                if (clavesDelDiccionario.Contains(clave))
                {
                    float number;

                    if (Single.TryParse(valor, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out number))
                    {
                        //Debug.Log(String.Format("clave: {0} : valor: {1}", clave, valor));
                        //Debug.Log(number);

                        dict[clave] = number;
                    }
                    else
                    {
                        Debug.LogWarning("no se pudo convertir el valor");
                        Debug.LogWarning(String.Format("clave: {0} : valor: {1}", clave, valor));
                    }
                }
                else
                {
                    Debug.LogWarning("no se encontro la clave");
                    Debug.LogWarning(String.Format("clave: {0} : valor: {1}", clave, valor));
                }
            }
            else
            {
                Debug.LogWarning("ah ocurrido un error en dividir el grupo");

            }

        }
        foreach (string key in clavesDelDiccionario)
        {
            Debug.Log(dict[key]);
        }
        //fin de start
    }


}


//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class Instancias: MonoBehaviour
{
    public GameObject box;
    // Start is called before the first frame update
    public Vector3 min = new Vector3(-5.0f, -5.0f, -5.0f);
    public Vector3 max = new Vector3(5.0f, 5.0f, 5.0f);

    void Start()
    {
        Instantiate(box, transform.position, transform.rotation) ;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
        var target = new Vector3(
          UnityEngine.Random.Range(min.x, max.x),
          UnityEngine.Random.Range(min.y, max.y),
          UnityEngine.Random.Range(min.z, max.z)
         );
        Instantiate(box, target, Random.rotation); //Quaternion.identity);

         }
    }
}
